package com.alk.javarush.service;

import com.alk.javarush.model.User;
import com.alk.javarush.repository.JpaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alk on 16.02.16.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private JpaUserRepository jpaUserRepository;

    public List<User> getAll() {
        return jpaUserRepository.getAll();
    }

    public User saveOrUpdate(User user) {
        return jpaUserRepository.saveUpdate(user);
    }

    public User delete(Integer id) {
        return jpaUserRepository.delete(id);
    }
}
