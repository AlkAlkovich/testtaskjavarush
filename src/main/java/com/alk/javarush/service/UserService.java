package com.alk.javarush.service;

import com.alk.javarush.model.User;

import java.util.List;

/**
 * Created by alk on 16.02.16.
 */
public interface UserService {

    List<User> getAll();
    User saveOrUpdate(User user);
    User delete(Integer id);
}
