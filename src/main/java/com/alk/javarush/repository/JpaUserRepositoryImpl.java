package com.alk.javarush.repository;

import com.alk.javarush.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by alk on 16.02.16.
 */
@Repository
@Transactional
public class JpaUserRepositoryImpl implements JpaUserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<User> getAll() {
        return entityManager.createNamedQuery(User.ALL_SORTED, User.class).getResultList();
    }

    public User saveUpdate(User user) {
        if (user.getId() == null) {
            entityManager.persist(user);
            entityManager.refresh(user);
        } else {
            entityManager.merge(user);
        }
        return user;
    }

    public User delete(Integer id) {
        User user = entityManager.find(User.class, id);
        if (user != null) {
            entityManager.remove(user);
        }
        return user;
    }
}
