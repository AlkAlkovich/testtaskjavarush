package com.alk.javarush.repository;

import com.alk.javarush.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by alk on 16.02.16.
 */
public interface JpaUserRepository {
    List<User> getAll();
    User saveUpdate(User user);
    User delete(Integer id);

}
