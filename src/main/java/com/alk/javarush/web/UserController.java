package com.alk.javarush.web;

import com.alk.javarush.model.User;
import com.alk.javarush.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by alk on 16.02.16.
 */
@RestController
public class UserController {
    @Autowired
    private UserServiceImpl service;

//    @RequestMapping( method = RequestMethod.GET)
//    public String userList(Model model) {
//        model.addAttribute("users", service.getAll());
//        return "userList";
//    }


    @RequestMapping(value = "/users/add/", method = RequestMethod.POST)
    public
    @ResponseBody
    User save(@RequestBody User user) {
        service.saveOrUpdate(user);
        return user;
    }
    @RequestMapping(value = "/users/add/", method = RequestMethod.PUT)
    public
    @ResponseBody
    User update(@RequestBody User user) {
        service.saveOrUpdate(user);
        return user;
    }

    @RequestMapping(value = "/users/delete/{id}", method = RequestMethod.DELETE)
    public
    @ResponseBody
    User delete(@PathVariable("id") int id) {
        return service.delete(id);
    }

    @RequestMapping(value = "/users/", method = RequestMethod.GET)
    public ResponseEntity<List<User>> listAllUsers() {
        List<User> users = service.getAll();
        if (users.isEmpty()) {
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }
}
