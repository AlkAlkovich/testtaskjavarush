<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html ng-app="helloAjaxApp">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Title</title>
    <link rel="stylesheet"
          href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.20/angular.min.js"></script>
    <%--<script src="https://hello-angularjs.appspot.com/assets/js/ui-bootstrap-tpls-0.9.0.min.js"></script>--%>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/1.1.2/ui-bootstrap.js"></script>

    <script>
        var helloAjaxApp = angular.module("helloAjaxApp", ['ui.bootstrap']).filter('startFrom', function(){
            return function(input, start){
                start = +start;
                return input.slice(start);
            }
        }).filter("filterByFields", function(){

            /**
             * @param items - Список фильтруемых записей
             * @param search - Искомая подстрока
             * @param items - список имен полей, по которым идет поиск
             */
            return function (items, search, fields)
            {
                if (!search)
                    return items;

                if (fields.length == 0)
                    throw Error("You have to define list of fields");

                search = (''+search).toLowerCase();

                var test = function(el, idx, array)
                {
                    var compare = function(val, search)
                    {
                        // null hasn't method toString()
                        if ( val == null )
                            return false;
                        return val.toString().toLowerCase().indexOf(search) !== -1;
                    };

                    var result = false;
                    var len = fields.length;
                    for (var i = 0; i < len; i++)
                    {
                        if (compare(el[fields[i]], search))
                        {
                            result = true;
                            break;
                        }
                    }
                    return result;
                };

                return items.filter(test);
            };
        }).filter('timestampToDate', function () {
            return function (timestamp) {
                var date = new Date(timestamp);
//                var dateObject = date.getFullYear() +'/'+ ('0' + (date.getMonth() + 1)).slice(-2) +'/'+ ('0' + date.getDate()).slice(-2);
                return date;
            };
        });;
        ;
        helloAjaxApp.controller("CompaniesCtrl", ['$scope', '$http', function ($scope, $http) {

            $scope.showModal = false;
            var idUser;
            $scope.edit = function (u) {
                idUser = u.id;

                $scope.showModal = !$scope.showModal;
                $scope.s={
                    name: u.name,
                    age: u.age,
                    isAdmin: u.isAdmin
                }
            };
            $scope.update=function(s){
                alert($scope.s.name);
                var dataObj = {
                    id:idUser,
                    name: $scope.s.name,
                    age: $scope.s.age,
                    isAdmin: $scope.s.isAdmin
                };

                var res = $http.put('http://localhost:8080/jr/users/add/', JSON.stringify(dataObj));
                res.success(function (data, status, headers, config) {
                    $scope.message = $scope.message = "User with id="+data.id + " updated.";
                    for (var i = 0; i < $scope.users.length; i++) {
                        if ($scope.users[i].id === idUser) {
                            $scope.users[i] = data;
                            break;
                        }
                    }
                });
            };

            $scope.users = [
                {
                    'name': 'alk',
                    'age': 12,
                    'isAdmin': 'true'
                }];
            var ress = $http.get('http://localhost:8080/jr/users/');
            ress.success(function (data, status, headers, config) {
                $scope.users = data;
            });

//================REMOVE=========================
            $scope.remove = function (ids) {
                var del = $http.delete('http://localhost:8080/jr/users/delete/' + ids);
                del.success(function (data, status, headers, config) {
                    $scope.message = $scope.message = "User with id="+data.id + " removed.";
                    var index = -1;
                    var comArr = eval($scope.users);
                    for (var i = 0; i < comArr.length; i++) {
                        if (comArr[i].id === ids) {
                            index = i;
                            break;
                        }
                    }
                    if (index === -1) {
                        alert("Something gone wrong");
                    }
                    $scope.users.splice(index, 1);
                });

            };
//===================ADD===========================
            $scope.addRowAsyncAsJSON = function () {
                ;
                // Writing it to the server
                //
                var dataObj = {
                    name: $scope.name,
                    age: $scope.age,
                    isAdmin: $scope.isAdmin
                };

                var res = $http.post('http://localhost:8080/jr/users/add/', JSON.stringify(dataObj));
                res.success(function (data, status, headers, config) {
                    $scope.message = "User "+data.name + " added.";
                    $scope.users.push({
                        'id': data.id,
                        'name': data.name,
                        'age': data.age,
                        'isAdmin': data.isAdmin,
                        'createDate':data.createDate});
                });
                res.error(function (data, status, headers, config) {
                    alert("failure message: " + JSON.stringify({data: data}));
                });
                // Making the fields empty
                //
                $scope.name = '';
                $scope.age = '';
                $scope.isAdmin = '';
            };
            $scope.currentPage = 0;
            $scope.itemsPerPage = 5;
            $scope.firstPage = function() {
                return $scope.currentPage == 0;
            }
            $scope.lastPage = function() {
                var lastPageNum = Math.ceil($scope.users.length / $scope.itemsPerPage - 1);
                return $scope.currentPage == lastPageNum;
            }
            $scope.numberOfPages = function(){
                return Math.ceil($scope.users.length / $scope.itemsPerPage);
            }
            $scope.startingItem = function() {
                return $scope.currentPage * $scope.itemsPerPage;
            }
            $scope.pageBack = function() {
                $scope.currentPage = $scope.currentPage - 1;
            }
            $scope.pageForward = function() {
                $scope.currentPage = $scope.currentPage + 1;
            }
        }]);

        helloAjaxApp.directive('modal', function () {
            return {
                template: '<div class="modal fade">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title }}</h4>' +
                '</div>' +
                '<div class="modal-body" ng-transclude></div>' +
                '</div>' +
                '</div>' +
                '</div>',
                restrict: 'E',
                transclude: true,
                replace: true,
                scope: true,
                link: function postLink(scope, element, attrs) {
                    scope.title = attrs.title;

                    scope.$watch(attrs.visible, function (value) {
                        if (value == true)
                            $(element).modal('show');
                        else
                            $(element).modal('hide');
                    });

                    $(element).on('shown.bs.modal', function () {
                        scope.$apply(function () {
                            scope.$parent[attrs.visible] = true;
                        });
                    });

                    $(element).on('hidden.bs.modal', function () {
                        scope.$apply(function () {
                            scope.$parent[attrs.visible] = false;
                        });
                    });
                }
            };
        });
    </script>
</head>

<body ng-controller="CompaniesCtrl">
<div style="padding-bottom:50px">
    <table style="width:100%">
        <tr>
            <td colspan="2" style="width:100%;padding:10px;">
                <div class="alert alert-info" role="alert">{{message}}</div>
            </td>
        </tr>
        <tr>
            <td style="width:45%;padding-left:40px;vertical-align: top">

                <div class="row">
                    <div class="col-md-3">
                        <div style="padding-bottom:20px">
                            <h3>Add User</h3>
                        </div>
                        <form class="form-horizontal" role="form" ng-submit="addRowAsyncAsJSON()">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="name"
                                           ng-model="name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Age</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="age"
                                           ng-model="age"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">isAdmin</label>
                                <div class="col-md-2">
                                    <input type="checkbox" ng-model="isAdmin"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div style="padding-left:170px">
                                    <input type="submit" value="Submit" class="btn btn-primary"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-9">
                        <div class="panel panel-default">
                            <!-- Default panel contents -->
                            <div class="panel-heading"><span class="lead">List of Users </span></div>
                            <div class="tablecontainer">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <%--<th>ID.</th>--%>
                                        <th>Name</th>
                                        <th>Age</th>
                                        <th>IsAdmin</th>
                                        <th>CreateDate</th>
                                        <%--<th width="20%"></th>--%>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="u in f=(users | filter:filter) | startFrom: startingItem() | limitTo: itemsPerPage">
                                        <%--<td><span ng-bind="u.id"></span></td>--%>
                                        <td><span ng-bind="u.name"></span></td>
                                        <td><span ng-bind="u.age"></span></td>
                                        <td><span ng-bind="u.isAdmin"></span></td>
                                        <td><span ng-bind="{{u.createDate | timestampToDate }}"></span></td>
                                        <td>
                                            <button type="button" ng-click="edit(u)" class="btn btn-success custom-width">Edit
                                            </button>
                                            <button type="button" ng-click="remove(u.id)" class="btn btn-danger custom-width">
                                                Remove
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <input placeholder="Filter by all columns" type="text" name="id" ng-model="filter">
                                </table>
                                <div id="pagination" class="row">
                                    <button class="pull-left btn btn-primary btn-sm" ng-disabled="firstPage()" ng-click="pageBack()">Назад</button>
                                    <span>{{currentPage+1}} из {{numberOfPages()}}</span>
                                    <button class="pull-right  btn btn-primary btn-sm" ng-disabled="lastPage()" ng-click="pageForward()">Вперед</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </td>

    </table>
    <modal title="User Edit" visible="showModal"  >
        <form hor role="form"  ng-submit="update(s)">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" ng-model="s.name" class="form-control" id="name"/>
            </div>
            <div class="form-group">
                <label for="age">Age</label>
                <input type="text" ng-model="s.age" class="form-control" id="age"/>
            </div>
            <div class="checkbox">
                <label for="isAdmin">IsAdmin</label>
                <input type="checkbox" ng-model="s.isAdmin" class="checkbox" id="isAdmin"/>
            </div>
            <button type="submit"  class="btn btn-default">Submit</button>
        </form>
    </modal>
</div>


</body>
</html>
