CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` int(11) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `isAdmin` bit(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (23,'2016-02-21 18:44:59',1,'User1');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (25,'2016-02-21 18:45:59',1,'User2');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (26,'2016-02-21 18:46:59',1,'User3');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (27,'2016-02-21 18:47:59',1,'User4');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (28,'2016-02-21 18:48:59',1,'User5');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (29,'2016-02-21 18:49:59',1,'User6');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (30,'2016-02-21 18:50:59',1,'User7');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (31,'2016-02-21 18:51:59',1,'User8');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (32,'2016-02-21 18:52:59',1,'User9');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (33,'2016-02-21 18:53:59',1,'User10');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (34,'2016-02-21 18:54:59',1,'User11');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (35,'2016-02-21 18:55:59',1,'User12');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (36,'2016-02-21 18:56:59',1,'User13');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (37,'2016-02-21 18:57:59',1,'User15');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (38,'2016-02-21 18:58:59',1,'User16');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (39,'2016-02-21 18:59:59',1,'User17');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (40,'2016-02-21 18:44:59',1,'User18');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (41,'2016-02-21 18:44:59',1,'User19');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (42,'2016-02-21 18:44:59',1,'User20');
INSERT INTO `test`.`users` (`age`,`createDate`,`isAdmin`,`name`) VALUES (43,'2016-02-21 18:44:59',1,'User21');